$(function(){

	var $win = $(window),
		body = $('body'),
		nav = $('.recommendNav'),
		navHeight = nav.outerHeight(),
		navPos = nav.offset().top,
		fixedClass = 'typeFixed';

$win.on('orientationchange', function() {
	navHeight = nav.outerHeight();
	navPos = nav.offset().top;
});

$win.on('load scroll orientationchange', function() {
	var value = $(this).scrollTop();
	if ( value > navPos ) {
		if($(window).height()>$(window).width()){
			nav.addClass(fixedClass);
			body.css('margin-top', navHeight);
		}
	} else {
		nav.removeClass(fixedClass);
		body.css('margin-top', '0');
	}
});

// 2019/07/23 #11576 T.Fukami add start ----------
$("article input[type=text]:not([name=kw]), article input[type=password]").focus(function(){
	nav.removeClass(fixedClass);
	body.css('margin-top', '0');
});
// 2019/07/23 #11576 T.Fukami add end   ----------

});//$(function()
