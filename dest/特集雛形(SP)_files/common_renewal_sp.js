//ページトップ
$(function() {
	"use strict";
    var topBtn = $('.pagetop');    
    topBtn.hide();
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {
        // lity(モーダル)表示中は常に隠す
        if ( $('body').find('.lity-opened').length > 0 ){
            topBtn.hide();
            return;
        }
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    //スクロールしてトップ
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});
$(window).on('load', function() {
  if (typeof fixedSidebar !== "undefined"){
    fixedSidebar.run();
  }
});
//スマホグローバルナビ
$(function(){
	$(".htop li.nav").on("click", function() {
		$(".gnavi").slideToggle();
	});
	$(".gnavi p").on("click", function() {
		$(".gnavi").slideToggle();
	});
	$(".gnavi ul li.parents").on("click", function() {
		$(this).next(".gnavi ul li.child").slideToggle();
		$(this).toggleClass("active");
	});
});
//topUseナビ
$(function(){
	$(".topuses dl dt").on("click", function() {
		$(this).next().slideToggle();
		$(this).toggleClass("active");
	});
});
//item説明
$(function(){
	$(".itemabout h3").on("click", function() {
		$(this).next(".itemabout div.box").slideToggle();
		$(this).toggleClass("active");
	});
});

//フッターナビ
$(function(){
	$("footer dl.fnavi dt").on("click", function() {
		$(this).next().slideToggle();
		$(this).toggleClass("active");
	});
});
//条件指定
$(function(){
	$(".termbtn").on("click", function() {
		$(".termbox").slideDown();
	});
	$(".termbox p.btnback").on("click", function() {
		$(".termbox").slideUp();
	});
});
//カフェ
$(function(){
	$(".styledl dt").on("click", function() {
		$(this).next().slideToggle();
		$(this).toggleClass("active");
		$(this).next.toggleClass("active");
	});
});
//検索リンク
$(function(){
	var headerHight = $('.recommendNav').height();
	$('a[href^=#]').click(function(){
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top - headerHight;
		$("html, body").animate({scrollTop:position}, 500, "swing");
	});
});
// 初期
function setPosition() {
	id = location.hash;
	if ( id ) {
		pos = $(id).offset().top - $('.recommendNav').height();
		$("html, body").animate({scrollTop:pos}, 500, "swing");
	}
}
/*
//テキストエリア自動調整
$(function(){
	$(document).on('input keyup blur', 'textarea', function() {
		var Tarea = $(this);
		if ( Tarea.get(0).scrollHeight > Tarea.get(0).offsetHeight ) {
			Tarea.height( Tarea.get(0).scrollHeight + "px" );
		}
	});
});
*/
