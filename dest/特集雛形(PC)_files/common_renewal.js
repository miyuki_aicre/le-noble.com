//ページトップ
$(function() {
	"use strict";
    var topBtn = $('.pagetop');    
    topBtn.hide();
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {
        // lity(モーダル)表示中は常に隠す
        if ( $('body').find('.lity-opened').length > 0 ){
            topBtn.hide();
            return;
        }
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    //スクロールしてトップ
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });

    //スクロールして対象の位置へ
    $(".jumpBtn").click(function () {
    	var target = $(this).attr("href");
    	var position = $(target).offset().top;
        $('body,html').animate({
            scrollTop: position - 20
        }, 600,"swing");
        return false;
    });
});

//$(window).on('load', function() {
//	if($('.fixnav').length>0){
//		//サイドバー固定
//		var fixedSidebar = (function() {
//		  var navi,
//		      wrap,
//		      wrap_scroll,
//		      fixed_start,
//		      fixpx_end_top;
//		  return {
//		    run : function() {
//		      // サイドバーの固定するレイヤー
//		      navi = $('.fixnav');
//		      // ラッパーのレイヤー
//		      wrap = $('.wrapper');
//		      this.refresh();
//		    },
//		    refresh : function() {
//		      navi.css({
//		        position : 'relative',
//		        top : 'auto'
//		      });
//		      var navi_top = navi.offset().top;
//		      var wrap_top = wrap.offset().top;
//		      // 開始位置：ナビのTOP
//		      fixed_start = navi_top - parseInt(navi.css('margin-top'));
//		      // スクロールする上限
//		      fixpx_end_top = wrap_top + wrap.outerHeight() - navi.outerHeight(true);
//		      wrap_scroll = fixpx_end_top;
//		      if(navi_top + navi.outerHeight(true) < wrap_top + wrap.outerHeight(true)) {
//		        $(window).off('scroll', _onScroll).on('scroll', _onScroll);
//		      } else {
//		        $(window).off('scroll', _onScroll);
//		      }
//		      $(window).trigger('scroll');
//		    }
//		  };
//		  function _onScroll() {
//		    var ws = $(window).scrollTop();
//		    if(ws > fixpx_end_top) {
//		      // 固定する上限
//		      navi.css({
//		        position : 'fixed',
//				width:'22.5%',
//		        top : (fixpx_end_top - ws) + 'px'
//		      });
//		    } else if(ws > fixed_start) {
//		      // 固定中間
//		      navi.css({
//		        position : 'fixed',
//				width:'22.5%',
//		        top : '0px'
//		      });
//		    } else {
//		      //　固定開始まで
//		      navi.css({
//		        position : 'relative',
//				width:'100%',
//		        top : '0px'
//		      });
//		    }
//		  }
//		})();
//		fixedSidebar.run();
//	}
//});