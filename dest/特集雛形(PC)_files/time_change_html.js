//
//onday,offdayに切り替えの対象クラス名を渡す。
//
//class = htmlを書き出すクラス名を引数で渡す
function time_change_html(ondayclass,offdayclass){
  
  var calObj = new Array();

  $("."+ondayclass+",."+offdayclass).hide();

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // [0]は0番目のカレンダーってことね。
  calObj[0] = new Object();


  // ["day"] 日付に対してのクラス指定
  calObj[0].daysClass = new Object();

  //下記、記入例。


  // セミコロンで区切ると説明コメント、URLを付けることができます(全クラス指定共通)。
  calObj[0].daysClass["8/12"] = 'Holyday;';
  calObj[0].daysClass["8/13"] = 'Holyday;';
  calObj[0].daysClass["8/14"] = 'Holyday;';
  calObj[0].daysClass["9/23"] = 'Holyday;';
  calObj[0].daysClass["11/4"] = 'Holyday;';
  calObj[0].daysClass["12/27"] = 'Holyday;';
  calObj[0].daysClass["12/28"] = 'Holyday;';
  calObj[0].daysClass["12/29"] = 'Holyday;';
  calObj[0].daysClass["12/30"] = 'Holyday;';
  calObj[0].daysClass["12/31"] = 'Holyday;';
  calObj[0].daysClass["1/1"] = 'Holyday;';
  calObj[0].daysClass["1/2"] = 'Holyday;';
  calObj[0].daysClass["1/3"] = 'Holyday;';
  calObj[0].daysClass["1/4"] = 'Holyday;';
  calObj[0].daysClass["1/5"] = 'Holyday;';


  calObj[0].daysClass["1/1"] = 'Holyday;元日';
  calObj[0].daysClass["2/11"] = 'Holyday;建国記念日';
  calObj[0].daysClass["4/29"] = 'Holyday;昭和の日';
  calObj[0].daysClass["5/3"] = 'Holyday;憲法記念日';
  calObj[0].daysClass["5/4"] = 'Holyday;みどりの日';
  calObj[0].daysClass["5/5"] = 'Holyday;こどもの日';
  calObj[0].daysClass["11/3"] = 'Holyday;文化の日';
  calObj[0].daysClass["11/23"] = 'Holyday;勤労感謝の日';
  calObj[0].daysClass["12/23"] = 'Holyday;天皇誕生日';

  // (○月) 第× △曜日の場合
  calObj[0].month = new Object();
  // 毎月「曜日-第○」 日:0 / 月:1 / 火:2 / 水:3 / 木:4 / 金:5 / 土:6
  //calObj[0].month["0-4"] = 'Holyday;第2月曜日'; // 第2火曜日はHolydayクラス指定

  // 固定月「月-曜日-第○」 日:0 / 月:1 / 火:2 / 水:3 / 木:4 / 金:5 / 土:6
  calObj[0].month["1-1-2"] = 'Holyday;成人の日'; // 1月 月曜日(1) 第2
  calObj[0].month["7-1-3"] = 'Holyday;海の日'; // 7月 月曜日(1) 第3
  calObj[0].month["9-1-3"] = 'Holyday;敬老の日'; // 9月 月曜日(1) 第3
  calObj[0].month["10-1-2"] = 'Holyday;体育の日'; // 10月 月曜日(1) 第2



  //営業時間を曜日ごとに設定
  calObj[0].onday = new Object();

  // 固定月「月-曜日-第○」 日:0 / 月:1 / 火:2 / 水:3 / 木:4 / 金:5 / 土:6
  //時間は「：」と「-」でつなぐ。offは休み
  calObj[0].onday[0] = '10:00-17:00';
  calObj[0].onday[1] = '10:00-18:00';
  calObj[0].onday[2] = '10:00-18:00';
  calObj[0].onday[3] = '10:00-18:00';
  calObj[0].onday[4] = '10:00-18:00';
  calObj[0].onday[5] = '10:00-10:00';
  calObj[0].onday[6] = 'off';
  calObj[0].onday["holliday"] = '10:00-17:00';


  //特定指定の休日
  calObj[0].offday = new Object();
  calObj[0].offday["7/23"] = "off";


  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  calObj.date = new Date();

  calObj.day = calObj.date.getDate();
  calObj.month = calObj.date.getMonth() + 1;
  calObj.year = calObj.date.getFullYear();
  calObj.weeknum = Math.floor((calObj.day + 6)/7);
  calObj.week = calObj.date.getDay();
  calObj.hours = ("0"+calObj.date.getHours()).slice(-2);
  calObj.minutes = ("0"+calObj.date.getMinutes()).slice(-2);

  calObj.dayWeew = calObj.month+"-"+calObj.week+"-"+calObj.weeknum;
  calObj.MonthDay = calObj.month+"/"+calObj.day;


  calObj.holliday ="";

  if(calObj[0].month[calObj.dayWeew]){
    calObj.holliday = calObj[0].month[calObj.dayWeew];

  }else if(calObj[0].daysClass[calObj.MonthDay]){
    calObj.holliday = calObj[0].daysClass[calObj.MonthDay];
  }

  if(calObj.holliday){
    calObj.key = "holliday";
  }else{
    calObj.key = calObj.week;
  }

  if(calObj[0].offday[calObj.MonthDay] || calObj[0].onday[calObj.key] == "off"){

    //休日の日
    $("."+offdayclass).show();

  }else{
    calObj.Time =  calObj[0].onday[calObj.key].split("-");
    calObj.Time_s = parseInt(calObj.Time[0].replace(":",""),10);
    calObj.Time_e = parseInt(calObj.Time[1].replace(":",""),10);

    calObj.Time = parseInt(calObj.hours + calObj.minutes,10);

    if((calObj.Time_s < calObj.Time) && (calObj.Time < calObj.Time_e )){
      //営業時間内
      $("."+ondayclass).show();

    }else{
      //時間外
      $("."+offdayclass).show();
    }

  }
}

