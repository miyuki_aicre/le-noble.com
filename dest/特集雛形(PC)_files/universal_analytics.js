/* get cookie array */
__cookies = GetCookies();
if ( !(__cookies['sp_flg']) ){
	__cookies['sp_flg'] = '';
}
/* debug */
document.write('<!-- Virtual Url Value for GoogleAnalytics, UserInsight -->');
document.write('<input type="hidden" name="__cookies_sp_flg" value="' + __cookies['sp_flg'] + '">');
__virtual_url = location.href;
__virtual_url = __virtual_url.replace(/le\-noble\.com\/cart\//g, 'le-noble.com/sp/cart/');
__virtual_url = __virtual_url.replace(/le\-noble\.com\/members\//g, 'le-noble.com/sp/members/');
__virtual_url = __virtual_url.replace(/le\-noble\.com\/mypage\//g, 'le-noble.com/sp/mypage/');
/* debug */
document.write('<input type="hidden" name="__virtual_url" value="' + __virtual_url + '">');

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-235941-1', 'auto');
ga('require', 'displayfeatures');
ga('require', 'linkid', 'linkid.js');
if ( __cookies['sp_flg'] && __cookies['sp_flg'] == '1' ){
	ga('send', 'pageview', {
		'page': __virtual_url,
		'title': document.title
	});
}else{
	ga('send', 'pageview');
}

/* 2019/05/28 T.Fukami add start */
var __device = ( __cookies['sp_flg'] && __cookies['sp_flg'] == '1' ? 'sp' : 'pc' );
var __pathname = location.pathname;
var __path = '';
if ( __pathname.indexOf('/cart/') !== -1 ){
	__path = 'cart';
}else if ( __pathname.indexOf('/mypage/') !== -1 ){
	__path = 'mypage';
}else if ( __pathname.indexOf('/favorites_add') !== -1 ){
	__path = 'favorites';
}else{
	__path = __pathname;
}
if ( __cookies['login_email'] ){
	if ( __cookies['login_email'] == '1' ){
		/* Logined by Email  */
		/*ga('send', 'event', __device, 'login', 'email');*/
		ga('send', 'event', __path, 'login', 'email');
	}else if ( __cookies['login_email'] == '0' ){
		/* Logined by Member's No  */
		/*ga('send', 'event', __device, 'login', 'id');*/
		ga('send', 'event', __path, 'login', 'id');
	}
	/* Remove Cookie `login_email` */
	try {
		$.removeCookie("login_email", { path: "/" });
	}catch(e){
		if (navigator.cookieEnabled) {
			var __date = new Date();
			__date.setTime( __date.getTime() - 1 );
			document.cookie = "login_email=; domain = .le-noble.com; path = /; max-age=1; expires=" + __date.toUTCString();
		}
	}
}
/* 2019/05/28 T.Fukami add end   */

/* User Insight PCDF Code Start : le-noble.com */
var _uic = _uic ||{}; var _uih = _uih ||{};_uih['id'] = 50686;
_uih['lg_id'] = GetValue();
_uih['fb_id'] = '';
_uih['tw_id'] = '';
_uih['uigr_1'] = ''; _uih['uigr_2'] = ''; _uih['uigr_3'] = ''; _uih['uigr_4'] = ''; _uih['uigr_5'] = '';
_uih['uigr_6'] = ''; _uih['uigr_7'] = ''; _uih['uigr_8'] = ''; _uih['uigr_9'] = ''; _uih['uigr_10'] = '';
if ( __cookies['sp_flg'] && __cookies['sp_flg'] == '1' ){
	_uih['url'] = __virtual_url;
}

/* DO NOT ALTER BELOW THIS LINE */
/* WITH FIRST PARTY COOKIE */
(function() {
var bi = document.createElement('scri'+'pt');bi.type = 'text/javascript'; bi.async = true;
bi.src = ('https:' == document.location.protocol ? 'https://bs' : 'http://c') + '.nakanohito.jp/b3/bi.js';
var s = document.getElementsByTagName('scri'+'pt')[0];s.parentNode.insertBefore(bi, s);
})();
/* User Insight PCDF Code End : le-noble.com */

/* le-noble.com Customize Code Start */
function GetValue()
{
	var result = null;
	//elm = document.getElementById('__le_mem_id');
	//if ( elm ){
	//	 result = elm.value;
	//}
	if ( __cookies['_le_o_mem_id'] ){
		result = __cookies['_le_o_mem_id'];
	}else{
		result = '';
	}
	//document.write('<input type="hidden" name="__debug" value="' + result + '">');
	return result;
}

function GetCookies()
{
	var result = new Array();
	var allcookies = document.cookie;
	if( allcookies != '' ){
		var cookies = allcookies.split( '; ' );
		for( var i = 0; i < cookies.length; i++ ){
			var cookie = cookies[ i ].split( '=' );
			result[ cookie[0] ] = decodeURIComponent( cookie[1] );
		}
	}
	return result;
}
/* le-noble.com Customize Code End */
