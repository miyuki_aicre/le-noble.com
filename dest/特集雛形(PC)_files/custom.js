$('.slidebnr').not('.slick-initialized').slick({
	arrows:false,
	infinite: true,
	slidesToShow: 5,
	slidesToScroll: 1,
	autoplay: true,
	cssEase: 'ease',
	dots:true,
	speed: 1000,
	autoplaySpeed: 2000,
	pauseOnFocus:false
	/*
	arrows:false,
	infinite: true,
	slidesToShow: 5,
	slidesToScroll: 1,
	autoplay: true,
	cssEase: 'linear',
	speed: 6000,
	autoplaySpeed: 0,
	pauseOnFocus:false
	*/
});

$('.campaginbnr').not('.fixed').not('.slick-initialized').slick({
	arrows:false,//矢印
	infinite: true,//無限
	autoplay:true,//自動再生
	dots: true,
	cssEase: 'ease',//イージングなし
	speed: 2000,//動きのスピードミリ秒
	autoplaySpeed: 3000,//停止時間ミリ秒数
	slidesToShow: 4,//一個だけ表示
	centerMode: true,
	variableWidth: true,
	pauseOnFocus:false
});

$('.featurebnr').not('.slick-initialized').slick({
	arrows:true,//矢印
	infinite: true,//無限
	autoplay:true,//自動再生
	dots: false,
	cssEase: 'ease',//イージングなし
	speed: 1000,//動きのスピードミリ秒
	autoplaySpeed: 3000,//停止時間ミリ秒数
	fade: true,
	slidesToShow: 1,//一個だけ表示
	slidesToScroll: 1//一個だけスクロール
});
$('.thumimg').not('.slick-initialized').slick({
	arrows:true,//矢印
	infinite: true,//無限
	autoplay:false,//自動再生
	dots: true,
	cssEase: 'ease',//イージングなし
	speed: 500,//動きのスピードミリ秒
	slidesToShow: 1,//一個だけ表示
	slidesToScroll: 1,//一個だけスクロール
});
jQuery(function($){
	if($(".mainimg img").length>0){
	$(".mainimg img").bind("load",function(){
		var ImgHeight = $(this).height();
		$('.mainimg').css('height',"100%");
	});
	
	$('.thumimg a').click(function(){
		if($(this).hasClass('over') == false){
			$('.thumimg a').removeClass('over');
			$(this).addClass('over');
			$('.mainimg img').hide().attr('src',$(this).attr('href')).fadeIn();
		};
		return false;
	});
	if($('.thumimg ul.slick-active li a').length > 0){
		$('.thumimg ul.slick-active li:eq(1) a').click();
	}else{
		$('.thumimg ul.slick-active li:eq(0) a').click();
	}
	}
});
$('.otherslide').not('.slick-initialized').slick({
	arrows:true,//矢印
	infinite: true,//無限
	autoplay:false,//自動再生
	dots: true,
	cssEase: 'ease',//イージングなし
	speed: 1000,//動きのスピードミリ秒
	slidesToShow: 5,//一個だけ表示
	slidesToScroll: 5,//一個だけスクロール
});
$('.otheritems').not('.slick-initialized').slick({
	arrows:true,//矢印
	infinite: true,//無限
	autoplay:false,//自動再生
	dots: true,
	cssEase: 'ease',//イージングなし
	speed: 2000,//動きのスピードミリ秒
	slidesToShow: 5,//一個だけ表示
	slidesToScroll: 1,//一個だけスクロール
});
